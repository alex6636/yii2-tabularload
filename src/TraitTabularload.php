<?php
namespace alexs\yii2tabularload;
use yii\db\ActiveRecord;
use yii\helpers\StringHelper;

trait TraitTabularload
{
    /**
     * Drop rows that have only empty values
     *
     * @return bool
     */
    public static function tabularloadFilterValues() {
        return true;
    }

    /**
     * @return array
     */
    public static function tabularloadGetFilteredValues() {
        return [[], '', null];
    }

    /**
     * The primary key
     *
     * @return string
     */
    public static function tabularloadGetPkAttr() {
        return 'id';
    }

    /**
     * We must ensure the data has been filled manually before create an item.
     * When we have only text inputs and/or text areas this property does not make sense.
     * But when we have hidden fields and/or checkboxes, radios, etc.
     * we need to check they have default values or not.
     * If item has only default values except anything else it will be skipped.
     * For example:
     * $tabularload_default_values = ['hidden_field'=>'default_value', 'checkbox_field'=>'0']
     * The comparison is strict.
     *
     * @return array
     */
    public static function tabularloadGetDefaultValues() {
        return [];
    }

    /**
     * Loading models when we create them
     *
     * @param string $model_name
     * for example: 'app\models\City'
     * @param array $behaviors behaviors which have dynamic attributes
     * @return ActiveRecord[]
     */
    public static function tabularloadCreate($model_name, array $behaviors = []) {
        $models = [];
        $data = static::tabularloadGetData($model_name);
        if (static::tabularloadFilterValues()) {
            $data = array_filter(array_map(function($_data) {
                return array_filter($_data, [__CLASS__, 'tabularloadFilter']);
            }, $data), [__CLASS__, 'tabularloadFilter']);
        }
        if (!empty($data)) {
            foreach ($data as $i=>$_data) {
                if (static::tabularloadIsCreatedItemFilled($_data)) {
                    /** @var ActiveRecord $model */
                    $model = new $model_name;
                    $model->load($_data, '');
                    static::tabularloadAttachBehaviors($model, $i, $behaviors);
                    $models[] = $model;
                }
            }
        }
        return $models;
    }

    /**
     * Loading models when we update them (they are exists)
     *
     * @param string $model_name
     * for example: 'app\models\City'
     * @param string|array|null $related_model_condition
     * for example: ['country_id'=>$country_id]
     * @param array $behaviors behaviors which have dynamic attributes
     * @param bool $throw_exception
     * @return ActiveRecord[]
     * @throws \InvalidArgumentException
     */
    public static function tabularloadUpdate($model_name, $related_model_condition = null, array $behaviors = [], $throw_exception = false) {
        $models = [];
        $data = static::tabularloadGetData($model_name);
        $pk_attr = static::tabularloadGetPkAttr();
        // remove deleted models
        $ids = array_column($data, $pk_attr);
        /** @var ActiveRecord $model_name */
        if ($related_model_condition) {
            $model_name::deleteAll(['AND', $related_model_condition, ['NOT IN', $pk_attr, $ids]]);
        } else {
            $model_name::deleteAll(['NOT IN', $pk_attr, $ids]);
        }
        if (!empty($data)) {
            foreach ($data as $i=>$_data) {
                /** @var ActiveRecord $model */
                if (!empty($_data[$pk_attr])) {
                    if (!$model = $model_name::findOne($_data['id'])) {
                        if ($throw_exception) {
                            throw new \InvalidArgumentException;
                        }
                        // just skip the model
                        continue;
                    }
                    $model->load($_data, '');
                    static::tabularloadAttachBehaviors($model, $i, $behaviors);
                    $models[] = $model;
                } else {
                    if (static::tabularloadFilterValues()) {
                        $_data = array_filter($_data, [__CLASS__, 'tabularloadFilter']);
                    }
                    if (!empty($_data)) {
                        if (static::tabularloadIsCreatedItemFilled($_data)) {
                            $model = new $model_name;
                            $model->load($_data, '');
                            static::tabularloadAttachBehaviors($model, $i, $behaviors);
                            $models[] = $model;
                        }
                    }
                }
            }
        }
        return $models;
    }

    /**
     * For example:
     * City::tabularloadSave($cities, 'country', $country)
     * City model should have a link
     * public function getCountry() {
     *     return $this->hasOne(Country::class, ['id'=>'country_id']);
     * }
     *
     * @param ActiveRecord[] $models
     * @param string|null $link_name
     * @param ActiveRecord|null $link_model
     */
    public static function tabularloadSave(array $models, $link_name = null, $link_model = null) {
        if (!empty($models)) {
            foreach ($models as $model) {
                if ($link_name && $link_model) {
                    $model->link($link_name, $link_model);
                }
                $model->save(false);
            }
        }
    }

    /**
     * @param string $model_name
     * for example: 'app\models\City'
     * @return array
     */
    public static function tabularloadGetData($model_name) {
        $basename = StringHelper::basename($model_name);
        return (array) \Yii::$app->request->post($basename);
    }

    /**
     * The callback for the array_filter method
     *
     * @param mixed $var
     * @return bool
     */
    public static function tabularloadFilter($var) {
        return !in_array($var, static::tabularloadGetFilteredValues(), true);
    }

    /**
     * Attach behaviors which have dynamic attribute names
     *
     * @param ActiveRecord $model
     * @param int $i an index of the model in the post array
     * @param array $behaviors behaviors which have dynamic attributes
     * For example [
     * [
     *     [
     *         'class'=>Fileable::class,
     *         'upload_dir'=>City::getUploadDir(),
     *     ],
     *     ['attribute'=>'file1'],
     *     [...'Fileable1'],
     * ],
     * [
     *     [
     *         'class'=>Fileable::class,
     *         'upload_dir'=>City::getUploadDir(),
     *     ],
     *     ['attribute'=>'file2'],
     * ],
     * ]
     */
    protected static function tabularloadAttachBehaviors(ActiveRecord $model, $i, array $behaviors = []) {
        if (!empty($behaviors)) {
            foreach ($behaviors as $_i=>$_behavior) {
                list($behavior, $attributes) = $_behavior;
                $behavior_name = isset($_behavior[2]) ? $_behavior[2] : $_i;
                foreach ($attributes as $attribute_name=>$attribute) {
                    $behavior[$attribute_name] = "[$i]$attribute";
                }
                $model->attachBehavior($behavior_name, $behavior);
            }
        }
    }

    /**
     * Checking out the data after filtering before item creation.
     *
     * @param array $data
     * @return bool
     */
    protected static function tabularloadIsCreatedItemFilled(array $data) {
        $default_values = static::tabularloadGetDefaultValues();
        if (empty($default_values)) {
            return true;
        }
        $is_multidimensional = count($default_values) !== count($default_values, COUNT_RECURSIVE);
        if ($is_multidimensional) {
            $diff = array_diff(array_map('serialize', $data), array_map('serialize', $default_values));
        } else {
            $diff = array_diff_assoc($data, $default_values);
        }
        return !empty($diff);
    }
}