<?php
namespace alexs\yii2tabularload\tests\models;
use alexs\yii2tabularload\TraitTabularload;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $country_id
 * @property string $name
 * @property int $is_capital
 * @property int $foundation
 * @property string $image
 * @property Country $course
 */

class City extends ActiveRecord
{
    use TraitTabularload;

    public function rules() {
        return [
            [['name', 'foundation'], 'filter', 'filter'=>'trim'],
            [['name', 'foundation'], 'required'],
            [['is_capital', 'foundation'], 'integer'],
            //['image', 'image', 'extensions'=>['jpg', 'jpeg', 'png', 'gif']],
            ['image', 'string'], // just for tests
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry() {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }
}