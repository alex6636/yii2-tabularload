<?php
namespace alexs\yii2tabularload\tests\models;

class CityWithDefaultFormValues extends City
{
    public static function tabularloadGetDefaultValues() {
        return [
            'is_capital' => '0', // checkbox sends the value '0' to the server by default
        ];
    }

    public static function tableName() {
        return 'city';
    }
}