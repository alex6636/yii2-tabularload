<?php
namespace alexs\yii2tabularload\tests\models;

class CityWithFilterDisabled extends City
{
    public static function tabularloadFilterValues() {
        return false;
    }

    public static function tableName() {
        return 'city';
    }
}