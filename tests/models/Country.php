<?php
namespace alexs\yii2tabularload\tests\models;
use yii\db\ActiveRecord;


/**
 * @property int $id
 * @property string $name
 * @property int $population
 */

class Country extends ActiveRecord
{
    public function rules() {
        return [
            [['name', 'population'], 'filter', 'filter'=>'trim'],
            [['name', 'population'], 'required'],
            ['population', 'integer'],
        ];
    }
}